package com.library.controller;

import com.library.entity.BookWithUser;
import com.library.service.BookService;
import com.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/books/")
public class BookApiController {

    @Autowired
    BookService bookService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Collection<BookWithUser> books(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "author") String sorting, @RequestParam(defaultValue = "ASC") String order) {
        return bookService.getAllBooksSortedAndPaged(sorting, order, page).stream().map(b -> bookService.getBookWithUser(b)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/take/{id}", method = RequestMethod.GET)
    public void takeBook(@PathVariable int id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        userService.takeBook(auth.getName(),id);
    }

    @RequestMapping(value = "/return/{id}", method = RequestMethod.GET)
    public void returnBook(@PathVariable int id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        userService.returnBook(auth.getName(),id);
    }


}
