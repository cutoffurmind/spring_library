package com.library.controller;

import com.library.forms.BookForm;
import com.library.forms.UserForm;
import com.library.service.BookService;
import com.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.validation.Valid;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
public class BookController {
    @Autowired
    private BookService bookService;

    @Autowired
    @Qualifier("bookFormValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(value = "/")
    public String index() {
        return "redirect:/books";
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public ModelAndView books() {
        ModelAndView model = new ModelAndView();
        model.addObject("books", bookService.getAllBooksSortedAndPaged("author", "ASC", 1).stream().map(book ->
            bookService.getBookWithUser(book)
        ).collect(Collectors.toList()));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addObject("username", auth.getName());
        model.addObject("pages", bookService.getPagesCount());
        model.setViewName("books");
        return model;
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ModelAndView updateOrCreateBook(@Valid BookForm bookForm, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView(new MappingJackson2JsonView());
        if(bindingResult.hasErrors()) {
            model.addObject("success", false);
            model.addObject("errors", bindingResult.getAllErrors());
        } else {
            switch (bookForm.getAction()) {
                case "update": bookService.updateBook(bookForm);break;
                case "create": bookService.insertBook(bookForm);break;
            }
            model.addObject("success", true);
        }
        return model;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable int id) {
        if(bookService.bookExists(id)) {
            bookService.removeBookById(id);
            return new ModelAndView("redirect:/books");
        } else {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
    }
}
