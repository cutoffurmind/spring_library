package com.library.controller;

import com.library.entity.User;
import com.library.forms.UserForm;
import com.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("userFormValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView users() {
        ModelAndView model = new ModelAndView();
        model.addObject("users", userService.getAllUsers());
        model.setViewName("users");
        return model;
    }

    @RequestMapping(value = "/users/validate", method = RequestMethod.GET)
    public ModelAndView validateUsername(@RequestParam String username) {
        ModelAndView model = new ModelAndView(new MappingJackson2JsonView());
        return model.addObject("result",userService.validUsername(username)?"true":"false");
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ModelAndView updateOrCreateUser(@Valid UserForm userForm, BindingResult bindingResult) {
        ModelAndView model = new ModelAndView(new MappingJackson2JsonView());
        if(bindingResult.hasErrors()) {
            model.addObject("success", false);
            model.addObject("errors", bindingResult.getAllErrors());
        } else {
            switch (userForm.getAction()) {
                case "update": userService.updateUser(userForm);break;
                case "create": userService.insertUser(userForm);break;
            }
            model.addObject("success", true);
        }
        return model;
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable int id) {
        if(userService.userExists(id)) {
            userService.removeUserById(id);
            return new ModelAndView("redirect:/users");
        } else {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
    }


}
