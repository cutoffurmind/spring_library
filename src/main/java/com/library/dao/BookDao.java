package com.library.dao;

import com.library.entity.Book;
import com.library.entity.User;

import java.util.Collection;
import java.util.Optional;

public interface BookDao {
    Collection<Book> getAllBooks();

    Collection<Book> getAllBooksSortedAndLimited(String sortingColumn, String order, int firstRow, int lastRow);

    int getBooksCount();

    Optional<Book> getBookById(int id);

    void removeBookById(int id);

    void updateBook(Book book);

    void insertBook(Book book);

    public Optional<User> getBookUser(Book book);

    boolean validISN(String isn);
}
