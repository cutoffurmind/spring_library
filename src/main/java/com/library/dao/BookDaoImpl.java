package com.library.dao;

import com.library.entity.Book;
import com.library.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

@Repository
public class BookDaoImpl implements BookDao {
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Override
    public Collection<Book> getAllBooks() {
        String sql = "SELECT * FROM books";
        return jdbcTemplate.query(sql, new Object[] {}, new BookMapper());
    }
    @Override
    public Collection<Book> getAllBooksSortedAndLimited(String sortingColumn, String order, int firstRow, int lastRow) {
        if(!sortingColumn.equals("author") && !sortingColumn.equals("name"))
            sortingColumn = "author";

        if(!order.equals("ASC") && !order.equals("DESC"))
            order = "ASC";

        String sql = "SELECT * FROM books ORDER BY " + sortingColumn + " " + order + " LIMIT ?,?";
        return jdbcTemplate.query(sql, new Object[] { firstRow, lastRow}, new BookMapper());
    }

    @Override
    public int getBooksCount() {
        String sql = "SELECT COUNT(*) FROM books";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public Optional<Book> getBookById(int id) {
        try {
            String sql = "SELECT * FROM books WHERE id = ?";
            Book b = jdbcTemplate.queryForObject(sql, new Object[]{id}, new BookMapper());
            return Optional.of(b);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void removeBookById(int id) {
        String sql = "DELETE FROM books WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void updateBook(Book book) {
        String sql = "UPDATE books SET ISN= ?, author= ?, name = ? WHERE id = ?";
        jdbcTemplate.update(sql, book.getISN(), book.getAuthor(), book.getName(), book.getId());
    }

    @Override
    public void insertBook(Book book) {
        String sql = "INSERT INTO books(ISN, author, name) VALUES (?,?,?)";
        jdbcTemplate.update(sql, book.getISN(), book.getAuthor(), book.getName());
    }


    @Override
    public Optional<User> getBookUser(Book book) {
        try {
            String sql = "SELECT " +
                    "users.id, users.username, users.password, books.id as book_id, books.ISN, books.author, books.name " +
                    "FROM users " +
                    "INNER JOIN user_books ON users.id = user_books.user_id " +
                    "AND user_books.book_id = ? " +
                    "INNER JOIN books ON user_books.book_id = books.id";
            User u = jdbcTemplate.queryForObject(sql, new Object[]{book.getId()}, new UserMapper());
            return Optional.of(u);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }

    }

    @Override
    public boolean validISN(String isn) {
        try {
            String sql = "SELECT * FROM books WHERE ISN = ?";
            Book u = jdbcTemplate.queryForObject(sql, new Object[] {isn}, new BookMapper());
            return false;
        } catch (EmptyResultDataAccessException e) {
            return true;
        }
    }


    private static final class BookMapper implements RowMapper<Book> {
        @Override
        public Book mapRow(ResultSet resultSet, int i) throws SQLException {
            Book book = new Book(resultSet.getInt("id"), resultSet.getString("ISN"), resultSet.getString("author"), resultSet.getString("name"));
            return book;
        }
    }
}
