package com.library.dao;

import com.library.entity.Book;
import com.library.entity.User;

import java.util.Collection;
import java.util.Optional;

public interface UserDao {
    Collection<User> getAllUsers();

    Optional<User> getUserById(int id);

    Optional<User> getUserByUsername(String username);

    void removeUserById(int id);

    void updateUser(User user);

    void insertUser(User user);

    boolean validUsername(String username);

    void takeBook(int userId, int bookId);

    void returnBook(int userId, int bookId);
}
