package com.library.dao;

import com.library.entity.User;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@Primary
public class UserDaoImpl implements UserDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public Collection<User> getAllUsers() {
        String sql = "SELECT * FROM users ORDER BY username ASC";
        return jdbcTemplate.query(sql, new Object[] {}, new UserMapper());
    }

    @Override
    public Optional<User> getUserById(int id) {
        try {
            String sql = "SELECT * FROM users WHERE id = ?";
            return Optional.of(jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserMapper()));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> getUserByUsername(String username) {
        try {
            String sql = "SELECT * FROM users WHERE username = ?";
            return Optional.of(jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserMapper()));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void removeUserById(int id) {
        String sql = "DELETE FROM users WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void updateUser(User user) {
        String sql = "UPDATE users SET username = ?, password = ? WHERE id = ?";
        jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), user.getId());
    }

    @Override
    public void insertUser(User user) {
        String sql = "INSERT INTO users(username, password) VALUES (?,?)";
        jdbcTemplate.update(sql, user.getUsername(), user.getPassword());
    }


    @Override
    public boolean validUsername(String username) {
        try {
            String sql = "SELECT * FROM users WHERE username = ?";
            User u = jdbcTemplate.queryForObject(sql, new Object[] {username}, new UserMapper());
            return false;
        } catch (EmptyResultDataAccessException e) {
            return true;
        }
    }


    @Override
    public void takeBook(int userId, int bookId) {
        String sql = "INSERT INTO user_books(user_id, book_id) VALUES (?, ?)";
        jdbcTemplate.update(sql, userId, bookId);
    }

    @Override
    public void returnBook(int userId, int bookId) {
        String sql = "DELETE FROM user_books WHERE user_id = ? AND book_id = ?";
        jdbcTemplate.update(sql, userId, bookId);
    }

}
