package com.library.entity;

public class Book {

    private int id;
    private String ISN;
    private String author;
    private String name;

    public Book() {}

    public Book(int id, String ISN, String author, String name) {
        this.id = id;
        this.ISN = ISN;
        this.author = author;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getISN() {
        return ISN;
    }

    public void setISN(String ISN) {
        this.ISN = ISN;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Book[id=" + id + ", ISN=" + ISN + ", author=" + author + ", name=" + name +"]";
    }

    @Override
    public boolean equals(Object o) {
        Book b = (Book)o;
        return b.getId() == id && b.getISN().equals(ISN) && b.getAuthor().equals(author) && b.getName().equals(name);
    }
}
