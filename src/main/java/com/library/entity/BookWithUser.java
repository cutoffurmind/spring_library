package com.library.entity;

import java.util.Optional;

public class BookWithUser {
    private Book book;
    private Optional<User> user;

    public BookWithUser() {}

    public BookWithUser(Book book, Optional<User> user) {
        this.book = book;
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Optional<User> getUser() {
        return user;
    }

    public void setUser(Optional<User> user) {
        this.user = user;
    }
}
