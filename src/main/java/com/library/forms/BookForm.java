package com.library.forms;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class BookForm {
    @NotNull
    @Min(1)
    private int id;

    @NotNull
    private String author;

    @NotNull
    private String name;

    @NotNull
    private String ISN;

    @NotNull
    @Pattern(regexp = "^(update|create)$", message = "invalid action")
    private String action;

    public BookForm() {}

    public BookForm(int id, String author, String name, String ISN, String action) {
        this.id = id;
        this.author = author;
        this.name = name;
        this.ISN = ISN;
        this.action = action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getISN() {
        return ISN;
    }

    public void setISN(String ISN) {
        this.ISN = ISN;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
