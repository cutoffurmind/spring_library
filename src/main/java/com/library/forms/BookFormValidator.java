package com.library.forms;

import com.library.entity.Book;
import com.library.entity.User;
import com.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class BookFormValidator implements Validator {

    @Autowired
    BookService bookService;

    @Override
    public boolean supports(Class<?> aClass) {
        return BookForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BookForm form = (BookForm) o;

        if(form.getAction().equals("update")) {
            Optional<Book> maybeBook = bookService.getBookById(form.getId());
            if(maybeBook.isPresent()) {
                if(!maybeBook.get().getISN().equals(form.getISN()) && !bookService.validISN(form.getISN()))
                    errors.rejectValue("ISN","exists");
            } else {
                errors.rejectValue("id","notfound");
            }
        } else {
            if(!bookService.validISN(form.getISN()))
                errors.rejectValue("ISN","exists");
        }
    }
}
