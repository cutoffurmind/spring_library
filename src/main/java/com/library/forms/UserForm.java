package com.library.forms;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserForm {

    @NotNull
    @Min(1)
    private int id;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    @Pattern(regexp = "^(update|create)$", message = "invalid action")
    private String action;

    public UserForm() {}

    public UserForm(int id, String username, String password, String action) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.action = action;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
