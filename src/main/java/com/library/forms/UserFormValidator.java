package com.library.forms;

import com.library.entity.User;
import com.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Optional;

@Component
public class UserFormValidator implements Validator {

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserForm form = (UserForm)o;

        if(form.getAction().equals("update")) {
            Optional<User> maybeUser = userService.getUserById(form.getId());
            if(maybeUser.isPresent()) {
                if(!maybeUser.get().getUsername().equals(form.getUsername()) && !userService.validUsername(form.getUsername()))
                    errors.rejectValue("username","exists");
            } else {
                    errors.rejectValue("id","notfound");
            }
        } else {
            if(!userService.validUsername(form.getUsername()))
                errors.rejectValue("username","exists");
        }

    }
}
