package com.library.service;

import com.library.dao.BookDao;
import com.library.entity.Book;
import com.library.entity.BookWithUser;
import com.library.entity.User;
import com.library.forms.BookForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class BookService {

    private static int BOOKS_PER_PAGE = 5;

    @Autowired
    private BookDao bookDao;

    public Collection<Book> getAllBooks() {
        return this.bookDao.getAllBooks();
    }

    public Collection<Book> getAllBooksSortedAndPaged(String column, String order, int page) {
        int firstRow = (page - 1)*BOOKS_PER_PAGE;
        int lastRow = page*BOOKS_PER_PAGE;
     return this.bookDao.getAllBooksSortedAndLimited(column, order,firstRow, lastRow);
    }

    public int getPagesCount() {
        return this.bookDao.getBooksCount() / BOOKS_PER_PAGE + 1;
    }

    public Optional<Book> getBookById(int id) {
        return this.bookDao.getBookById(id);
    }

    public void removeBookById(int id) {
        this.bookDao.removeBookById(id);
    }

    public void updateBook(BookForm bookForm) {
        Book book = new Book(bookForm.getId(), bookForm.getISN(), bookForm.getAuthor(), bookForm.getName());
        this.bookDao.updateBook(book);
    }

    public void insertBook(BookForm bookForm) {
        Book book = new Book(0, bookForm.getISN(), bookForm.getAuthor(), bookForm.getName());
        this.bookDao.insertBook(book);
    }

    public Optional<User> getBookUser(Book book) {
        return this.bookDao.getBookUser(book);
    }

    public BookWithUser getBookWithUser(Book book) {
        return new BookWithUser(book, getBookUser(book));
    }

    public boolean bookHasUser(Book book) {
        return getBookUser(book).isPresent();
    }

    public boolean bookExists(int id) {
        return getBookById(id).isPresent();
    }

    public boolean validISN(String isn) {
        return this.bookDao.validISN(isn);
    }
    
}
