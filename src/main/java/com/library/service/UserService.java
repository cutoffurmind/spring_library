package com.library.service;

import com.library.dao.BookDao;
import com.library.dao.UserDao;
import com.library.entity.Book;
import com.library.entity.User;
import com.library.forms.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class UserService {

    public UserService() {}

    @Autowired
    private UserDao userDao;

    @Autowired
    private BookDao bookDao;

    @Autowired
    private PasswordEncoder passwordEncoder;


    public Collection<User> getAllUsers() {
        return this.userDao.getAllUsers();
    }

    public Optional<User> getUserById(int id) {
        return this.userDao.getUserById(id);
    }

    public boolean userExists(int id) {
        return getUserById(id).isPresent();
    }

    public void removeUserById(int id) {
        this.userDao.removeUserById(id);
    }

    public void updateUser(UserForm userForm) {
        User user = new User(userForm.getId(), userForm.getUsername(), userForm.getPassword());
        Optional<User> prev = getUserById(userForm.getId());
        if(prev.isPresent()) {
            if(userForm.getPassword().equals("")) {
                user.setPassword(prev.get().getPassword());
            } else {
                user.setPassword(passwordEncoder.encode(userForm.getPassword()));
            }
        }

        this.userDao.updateUser(user);
    }

    public void insertUser(UserForm userForm) {
        User user = new User(0, userForm.getUsername(), passwordEncoder.encode(userForm.getPassword()));
        this.userDao.insertUser(user);
    }

    public boolean validUsername(String username) {
        return this.userDao.validUsername(username);
    }

    public void takeBook(String username, int bookId) {
        Optional<User> maybeUser = userDao.getUserByUsername(username);
        Optional<Book> maybeBook = bookDao.getBookById(bookId);
        if(maybeUser.isPresent() && maybeBook.isPresent())
            this.userDao.takeBook(maybeUser.get().getId(), bookId);
    }

    public void returnBook(String username, int bookId) {
        Optional<User> maybeUser = userDao.getUserByUsername(username);
        Optional<Book> maybeBook = bookDao.getBookById(bookId);
        if(maybeUser.isPresent() && maybeBook.isPresent())
            this.userDao.returnBook(maybeUser.get().getId(), bookId);
    }

    //public Optional<BookWithUser> getUserBooks
}
