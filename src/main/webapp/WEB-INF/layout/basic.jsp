<%@ page session="false" isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <tiles:getAsString name="title" />
        </title>
        <link href="<c:url value='/static/css/spectre.min.css' />" rel="stylesheet" />
        <link href="<c:url value='/static/css/spectre-exp.min.css' />" rel="stylesheet" />
        <link href="<c:url value='/static/css/spectre-icons.min.css' />" rel="stylesheet" />
        <link href="<c:url value='/static/css/main.css' />" rel="stylesheet" />
        <script src="<c:url value='/static/js/jquery-3.2.1.min.js' />"></script>
        <script src="<c:url value='/static/js/jquery.loadTemplate.min.js' />"></script>
    </head>
    <body>
        <div class="container">
            <header id ="header" class="navbar">
                <tiles:insertAttribute name="header" />
            </header>
            <div class="divider"></div>
            <section id="content">
                <tiles:insertAttribute name="body" />
            </section>
        </div>
    </body>
</html>