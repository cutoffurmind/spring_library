<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<input type="hidden" id="username" value="${username}">
<input type="hidden" id="pages" value="${pages}">

<h3 class="centered">Книги</h3>
<a href="#book-modal" id="add-book" class="btn btn-primary">Добавить книгу</a>
<div class="modal modal-sm" id="book-modal">
    <div class="modal-overlay" id="modals"></div>
    <div class="modal-container" role="document">
        <div class="modal-header">
            <div class="modal-title">Добавить Книгу</div>
        </div>
        <form id="book-form">
            <div class="modal-body">
                <div class="content centered">
                    <div class="toast toast-error" id="book-errors" style="display: none">

                    </div>
                    <!-- form input control -->
                    <div class="form-group">
                        <label class="form-label" for="isn">ISN</label>
                        <input class="form-input" type="text" id="isn" placeholder="ISN" name="isn" />
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="author">Автор</label>
                        <input class="form-input" type="text" id="author" placeholder="Автор" name="author" />
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="name">Название</label>
                        <input class="form-input" type="text" id="name" placeholder="Название" name="name" />
                    </div>
                    <input type="hidden" name="action" value="create">
                    <input type="hidden" name="id" value="0">
                    <input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" id="submit-btn">Добавить</button>
                    <button class="btn btn-link" type="reset">Отмена</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="table-container"></div>
<div class="loading"></div>
<script src="<c:url value='/static/js/books.js' />"></script>
<script type="text/html" id="table-row">
    <tr>
        <td><a href="#book-modal" class="edit-book-label" data-content="isn" data-id="id"></a></td>
        <td class="book-author" data-content="author"></td>
        <td class="book-name" data-content="name"></td>
        <td data-content="take">
        </td>
        <td><a href="" data-href="delete" class="confirm">Удалить</a></td>
    </tr>
</script>

<script type="text/html" id="table-template">
    <table class="table table-striped table-hover" id="books-table">
        <thead>
        <tr>
            <th>ISN</th>
            <th><a href="#" class="sortable" data-column="author" onclick="return changeSorting(this)">Автор</a></th>
            <th><a href="#" class="sortable" data-column="name" onclick="return changeSorting(this)">Название</a></th>
            <th>Кем взята</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tbody id="books-table-body">
        </tbody>
    </table>
    <button class="btn btn-primary centered" id="show-more">Показать еще</button>
</script>