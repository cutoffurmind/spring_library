<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" isELIgnored="false" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<h3 class="centered">Пользователи</h3>
<a href="#user-modal" class="btn btn-primary" id="add-user">Добавить пользователя</a>
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Имя пользователя</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><a href="#user-modal" class="edit-user-label" data-id="${user.id}">${user.username}</a></td>
            <td><a href="users/${user.id}" class="confirm">Удалить</a></td>
            <input type="hidden" id="user-${user.id}-json" value=""/>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="modal modal-sm" id="user-modal">
    <div class="modal-overlay" id="modals"></div>
    <div class="modal-container" role="document">
        <div class="modal-header">
            <div class="modal-title">Добавить пользователя</div>
        </div>
        <form id="user-form">
        <div class="modal-body">
            <div class="content centered">
                <div class="toast toast-error" id="user-errors" style="display: none">
                </div>
                    <!-- form input control -->
                    <div class="form-group">
                        <label class="form-label" for="username">Username</label>
                        <input class="form-input" type="text" id="username" placeholder="Username" name="usr" />
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="password">Пароль</label>
                        <input class="form-input" type="password" id="password" placeholder="Password" name="pwd" />
                    </div>
                    <input type="hidden" name="action" value="create">
                    <input type="hidden" name="id" value="0">
                    <input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            </div>
        </div>
        <div class="modal-footer">
            <div class="form-group">
                <button class="btn btn-primary" type="submit" id="submit-btn">Добавить</button>
                <button class="btn btn-link" type="reset">Отмена</button>
            </div>
        </div>
        </form>
    </div>
</div>
<script src="<c:url value='/static/js/users.js' />"></script>