var page = 1;
var sorting = 'author';
var order = 'ASC';

function changeSorting(elem) {
    var $prev = $('.selected-sorting');
    var dataColumn = $(elem).attr('data-column');
    if (dataColumn == $prev.attr('data-column')) {
        order = order == 'ASC' ? 'DESC' : 'ASC';
    } else {
        order = 'ASC';
        sorting =  dataColumn;
    }
    $('.sortable').each(function (el) {
       $(el).removeClass('selected-sorting').removeClass('sorting-asc').removeClass('sorting-desc');
    });
    initTable(function () {
        selectColumn(dataColumn);
    });


}

function selectColumn(column) {
    $('a[data-column=' + column + ']').addClass('selected-sorting').addClass('sorting-'+order.toLowerCase());
}

function updateHandlers() {
    // Подстановка данных книги в modal для редактирования

    $('.edit-book-label').unbind('click');

    $('.edit-book-label').click(function (e) {
        $('.modal-title').html('Редактировать книгу');
        $('#submit-btn').html('Редактировать');
        $('input[name=action]').val('update');
        $('input[name=isn]').val($(this).html());
        $('input[name=author]').val($(this).closest('tr').find('.book-author').html());
        $('input[name=name]').val($(this).closest('tr').find('.book-name').html());
        $('input[name=id]').val($(this).attr('id'));
    });

    $('.confirm').unbind('click');

    $('.confirm').click(function () {
        return confirm('Вы уверены?');
    });

    $('#show-more').unbind('click');

    if(parseInt($('#pages').val()) == page) {
        $('#show-more').hide();
    } else {
        $('#show-more').show();
        $('#show-more').click(function () {
            page = page + 1;
            loadTable(page, true);
        });
    }


}

function getTakeMarkup(bookId) {
    return '<a href="#" onclick="takeBook(' + bookId + ', this)" >Взять</a>';
}

function getReturnMarkup(bookId) {
    return '<a href="#" onclick="returnBook(' + bookId + ', this)" >Вернуть</a>';
}


function insertRows(books, append, callback) {
    $("#books-table-body").loadTemplate($("#table-row"), books, {append: append});
    updateHandlers();
    $('div .loading').hide();
    if(callback) {
        callback();
    }
}

function loadTable(p, append, callback) {
    page = p;
    $('div .loading').show();
    $.ajax({
        type: "GET",
        url: 'api/books/?sorting='+sorting+'&order='+order+'&page='+p,
        success: function(data)
        {
            var books = data.map(function (item) {
                var b = item.book;
                if(item.user) {
                    if($('#username').val() == item.user.username) {
                        b['take'] = getReturnMarkup(b.id);
                    } else {
                        b['take'] = item.user.username;
                    }
                } else {
                    b['take'] = getTakeMarkup(b.id);
                }
                b['delete'] = 'books/'+b.id;
                return b;
            });

            if(append) {
                insertRows(books, append, callback)
            } else {
                $('#table-container').loadTemplate($('#table-template'), {}, {
                    success: function () {
                        insertRows(books, append, callback);
                    }
                });
            }

        },
        error: function (data) {
            console.error(data);
        }
    });
}

function initTable(callback) {
    loadTable(1, false, callback);
}


function takeBook(bookId, elem) {
    $.ajax({
        type: "GET",
        url: 'api/books/take/'+bookId,
        success: function(data)
        {
            $(elem).html(getReturnMarkup(bookId))
        },
        error: function (data) {
            console.error(data);
        }
    });
    return false;
}

function returnBook(bookId, elem) {
    $.ajax({
        type: "GET",
        url: 'api/books/return/'+bookId,
        success: function(data)
        {
            $(elem).html(getTakeMarkup(bookId))
        },
        error: function (data) {
            console.error(data);
        }
    });
    return false;
}
$(document).ready(function () {
    initTable(function () {
        selectColumn('author');
    });
});
$(window).on('load', function(){

    // обнуление формы и закрытие модала
    $('button[type=reset]').click(function () {
        window.location.hash = '#modals';
        $('#book-errors').hide();
    });

    // Подстановка данных книги в modal для создания
    $('#add-book').click(function () {
        $('.modal-title').html('Добавить книгу');
        $('#submit-btn').html('Добавить');
        $('input[name=action]').val('create');
        $('input[name=id]').val('0');
    });

    // Обработка добавления/редактирования книги
    $('#book-form').submit(function (e) {
        e.preventDefault();
        var isn = $('#book-form input[name=isn]').val();
        var name = $('#book-form input[name=name]').val();
        var author = $('#book-form input[name=author]').val();
        var action = $('#book-form input[name=action]').val();
        var id = $('#book-form input[name=id]').val();
        if(isn == '' || name == '' || author == '') {
            $('#book-errors').html('').append('Заполните все поля').show();
            return;
        }
        var form = {
            action: action,
            name: name,
            author: author,
            ISN: isn,
            id: id
        };
        form[$('#csrf').attr('name')] = $('#csrf').val();
        $.ajax({
            type: "POST",
            url: 'books',
            data: form,
            success: function(data)
            {
                if(data.success) {
                    window.location = 'books';
                } else {
                    data.errors.forEach(function (elem) {
                        if(elem.field == 'ISN' && elem.code == 'exists') {
                            $('#book-errors').html('').append('Такой ISN уже существует').show();
                        }
                    });

                }
            },
            error: function (data) {

            }
        });
    })

});
