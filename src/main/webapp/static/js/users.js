$(window).on('load', function(){

    $('.confirm').on('click', function () {
        return confirm('Вы уверены?');
    });

    // обнуление формы и закрытие модала
    $('button[type=reset]').click(function () {
       window.location.hash = '#modals';
        $('#add-user-errors').hide();
    });

    // Подстановка данных пользователя в modal для редактирования
    $('.edit-user-label').click(function (e) {
        $('.modal-title').html('Редактировать пользователя');
        $('#submit-btn').html('Редактировать');
        $('input[name=action]').val('update');
        $('input[name=usr]').val($(this).html());
        $('input[name=id]').val($(this).attr('data-id'));
    });

    // Подстановка данных пользователя в modal для создания
    $('#add-user').click(function () {
        $('.modal-title').html('Добавить пользователя');
        $('#submit-btn').html('Добавить');
        $('input[name=action]').val('create');
        $('input[name=id]').val('0');
    });

    // Обработка добавления/редактирования пользователя
    $('#user-form').submit(function (e) {
        e.preventDefault();
        var usr = $('#user-form input[name=usr]').val();
        var pwd = $('#user-form input[name=pwd]').val();
        var action = $('#user-form input[name=action]').val();
        var id = $('#user-form input[name=id]').val();
        if(((usr == '' || pwd == '') && action == 'create') || (action == 'update' && usr == '')) {
            $('#add-user-errors').html('').append('Заполните все поля').show();
            return;
        }
        var form = {
            action: action,
            username: usr,
            password: pwd,
            id: id
        };
        form[$('#csrf').attr('name')] = $('#csrf').val();
        $.ajax({
            type: "POST",
            url: 'users',
            data: form,
            success: function(data)
            {
                if(data.success) {
                    window.location = 'users';
                } else {
                    data.errors.forEach(function (elem) {
                        if(elem.field == 'username' && elem.code == 'exists') {
                            $('#user-errors').html('').append('Такой username уже существует').show();
                        }
                    });

                }
            },
            error: function (data) {

            }
        });
    })

});
