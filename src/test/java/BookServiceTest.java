import com.library.config.TestDbConfig;
import com.library.entity.Book;
import com.library.entity.Book;
import com.library.forms.BookForm;
import com.library.service.BookService;
import com.library.service.BookService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@ContextConfiguration(classes = {TestDbConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ActiveProfiles("test")
public class BookServiceTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private BookService userService;

    @Autowired
    private BookService bookService;

    @Test
    public void books() {
        Assert.assertEquals(9, bookService.getAllBooks().size());
    }

    @Test
    public void insertBook() {
        Assert.assertEquals(9, bookService.getAllBooks().size());
        bookService.insertBook(new BookForm(0,"AUTHOR", "NAME", "ISN","create"));
        Assert.assertEquals(10, bookService.getAllBooks().size());
    }

    @Test
    public void removeBook() {
        bookService.removeBookById(1);
        Assert.assertEquals(8, bookService.getAllBooks().size());
    }

    @Test
    public void updateBook() {
        Optional<Book> book = bookService.getBookById(1);
        Assert.assertEquals("Book1", book.get().getName());
        userService.updateBook(new BookForm(book.get().getId(),"AUTHOR", "NAME", "ISN","update"));
        Optional<Book> updated = bookService.getBookById(1);
        Assert.assertEquals("NAME", updated.get().getName());
    }

}
