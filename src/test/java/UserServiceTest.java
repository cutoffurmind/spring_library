import com.library.config.TestDbConfig;
import com.library.entity.Book;
import com.library.entity.User;
import com.library.forms.UserForm;
import com.library.service.BookService;
import com.library.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@ContextConfiguration(classes = {TestDbConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ActiveProfiles("test")
public class UserServiceTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private BookService bookService;

    @Test
    public void insertUser() {
        Assert.assertEquals(3, userService.getAllUsers().size());
        userService.insertUser(new UserForm(0, "admin", "admin", "create"));
        Assert.assertEquals(4, userService.getAllUsers().size());
    }

    @Test
    public void getAllUsers() {
        Assert.assertEquals(3, userService.getAllUsers().size());
    }

    @Test
    public void removeUser() {
        userService.removeUserById(3);
        Assert.assertEquals(2, userService.getAllUsers().size());
    }

    @Test
    public void updateUser() {
        Optional<User> user = userService.getUserById(1);
        Assert.assertEquals("user", user.get().getUsername());
        userService.updateUser(new UserForm(user.get().getId(), "user123", "", "update"));
        Optional<User> updated = userService.getUserById(1);
        Assert.assertEquals("user123", updated.get().getUsername());
        Assert.assertEquals(user.get().getPassword(), updated.get().getPassword());
    }

    @Test
    public void takeReturnBook() {
        Optional<Book> book = bookService.getBookById(4);
        Assert.assertEquals(false, bookService.bookHasUser(book.get()));
        Optional<User> user = userService.getUserById(3);
        userService.takeBook(user.get().getUsername(), book.get().getId());
        Assert.assertEquals(true, bookService.bookHasUser(book.get()));
        userService.returnBook(user.get().getUsername(), book.get().getId());
        Assert.assertEquals(false, bookService.bookHasUser(book.get()));
    }

    @Test
    public void cascadeDeleteUser() {
        Optional<Book> book = bookService.getBookById(4);
        Optional<User> user = userService.getUserById(3);
        userService.takeBook(user.get().getUsername(), book.get().getId());
        Assert.assertEquals(true, bookService.bookHasUser(book.get()));
        userService.removeUserById(user.get().getId());
        Assert.assertEquals(false, bookService.bookHasUser(book.get()));
    }




}
